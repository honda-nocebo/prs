const path = require("path");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

const Dotenv = require("dotenv-webpack");

module.exports = (env, options) => {
  return {
    mode: "development",
    entry: "./src/js/index.tsx",
    output: {
      filename: "[name].js",
      path: path.join(__dirname, "public/js"),
      publicPath: "/js",
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          exclude: /node_modules/,
        },
        {
          test: /\.scss$/,
          use: [
            "style-loader",
            {
              loader: "css-loader",
              options: {
                modules: true,
              },
            },
            "sass-loader",
          ],
        },
        {
          test: /\.(glsl|vs|fs)$/,
          use: "ts-shader-loader",
        },
      ],
    },
    resolve: {
      extensions: [".tsx", ".ts", ".js"],
      plugins: [
        new TsconfigPathsPlugin({
          configFile: "tsconfig.json",
        }),
      ],
    },
    devtool: options.mode == "development" ? "inline-source-map" : "",
    plugins: [
      new Dotenv({
        path: path.join(__dirname, options.mode == "development" ? ".env" : ".env.production"),
        systemvars: true,
      }),
    ],
    devServer: {
      contentBase: "public",
      //   webpackのproxy重い？のでクロスドメインで直接やっちゃう
      //   proxy: {
      //     "/api": {
      //       target: "http://localhost:8888",
      //       changeOrigin: true,
      //       autoRewrite: true,
      //       //   pathRewrite: { "^/api": "" },
      //     },
      //   },
      watchOptions: {
        poll: true,
        ignored: /node_modules/,
      },
    },
  };
};
