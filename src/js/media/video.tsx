// =======================================
// video要素として配置はしてるけど
// 実際には参照してGLでcanvasに描画したもの表示している
// =======================================
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { updateValue } from "@/data";

import { Conf } from "@/index";

const createBlobURL = async (videoBuffer: Uint8Array) => {
  // console.log("loading");
  const mediaSource: any = new MediaSource();
  mediaSource.addEventListener("sourceopen", () => {
    const sourceBuffer = mediaSource.addSourceBuffer(`video/webm; codecs="opus, vp9"`);
    sourceBuffer.addEventListener("updateend", () => mediaSource.endOfStream());
    sourceBuffer.appendBuffer(videoBuffer);
  });
  const src = URL.createObjectURL(mediaSource);

  return new Promise((resolve) => {
    resolve(src);
  });
};

const Video: React.FC<any> = (props) => {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [blobUrl, setBlobUrl] = useState<any>();
  const { videoRef, setIsReady, videoBuffer } = props;

  useEffect(() => {
    !(async () => {
      const blob_url = [];
      for (let i = 0; i < Conf.VIDEO_TEXTURE_NUM; i++) {
        blob_url[i] = await createBlobURL(videoBuffer[i]);
      }
      setBlobUrl(blob_url);
      setIsLoaded(true);
    })();
    // return () => {// cleanup};
  }, []);

  useEffect(() => {
    let isCurrent = false;
    videoRef.forEach((video: any) => {
      isCurrent = !!video.current;
    });
    if (!isCurrent) return;
    videoRef[0].current.addEventListener("loadedmetadata", function () {
      updateValue.duration = videoRef[0].current.duration;
      setIsReady(true);
    });
  }, [isLoaded]);

  return isLoaded ? (
    <Root className="Video">
      {videoRef.map((v: any, i: number) => {
        return <video src={blobUrl[i]} ref={videoRef[i]} key={i} />;
      })}
    </Root>
  ) : null;
};

export default React.memo(Video);

const Root = styled.div`
  width: 640px;
  height: 360px;
  position: fixed;
  top: 0;
  right: 0;
  z-index: 10;
  visibility: hidden;
  /* noneだとtexture参照できない */
  video {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 640px;
    z-index: 1000;
  }
`;
