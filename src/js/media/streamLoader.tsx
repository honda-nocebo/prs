// =======================================
// サーバー配信
// 分割ロードとかシークバーなんかは未実装
// =======================================
import React, { useEffect, useCallback, useRef, useState } from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Conf } from "@/index";

const BASE_PATH = process.env.APP_URL + "/api/stream/";

let _contentLength = 0;
let _totalLength = 0;

const StreamLoader: React.FC<any> = (props) => {
  const [isLoadStart, setIsLoadStart] = useState<boolean>(false);
  const [percent, setPercent] = useState<number>(0);
  const [totalLength, setTotalLength] = useState<number>(0);
  const [currentLength, setCurrentLength] = useState<number>(0);
  const { setIsLoaded, setVideoBuffer } = props;

  const loadLength = async (request: string, id: string, quality: string): Promise<number> => {
    const formData = new FormData();
    formData.append("id", id);
    formData.append("quality", quality);
    formData.append("isReadLength", "true");
    const response: any = await fetch(request, {
      method: "POST",
      body: formData,
    });
    // const contentLength = +response.headers.get("Content-Length");
    if (!response.ok) return 0;
    const body = await response.json();
    const contentLength: number = +body;
    return new Promise((resolve) => {
      resolve(contentLength);
    });
  };
  const stream = async (request: string, id: string, quality: string) => {
    const formData = new FormData();
    formData.append("id", id);
    formData.append("quality", quality);
    const response: any = await fetch(request, {
      method: "POST",
      //   headers: { Range: "bytes=0-22344789" },
      //   分割する場合はこの辺でなんとかなりそうな
      body: formData,
    });
    const reader = response.body.getReader();

    let receivedLength = 0;
    const chunks = [];

    while (true) {
      const { done, value } = await reader.read();
      if (done) break;
      chunks.push(value);
      receivedLength += value.length;
      _contentLength += value.length;
      //   console.log(`Received ${receivedLength} of ${_contentLength}`);
    }
    const chunksAll = new Uint8Array(receivedLength);
    let position = 0;
    for (const chunk of chunks) {
      chunksAll.set(chunk, position);
      position += chunk.length;
    }
    const arrayBuffer = chunksAll;

    return new Promise((resolve) => {
      resolve(arrayBuffer);
    });
  };

  const loadStart = (quality: string) => {
    setIsLoadStart(true);
    // -------------
    // 場所がいまいちな気がするけどここでクオリティ設定
    Conf.QUALITY = quality;
    Conf.TEXTURE_WIDTH = Conf[Conf.QUALITY].TEXTURE_WIDTH;
    Conf.TEXTURE_HEIGHT = Conf[Conf.QUALITY].TEXTURE_HEIGHT;
    Conf.PARTICLE_COUNT = Conf[Conf.QUALITY].PARTICLE_COUNT;
    // -------------
    !(async () => {
      //   console.log("load start");
      for (let i = 0; i <= Conf.VIDEO_TEXTURE_NUM - 1; i++) {
        const l: number = await loadLength(BASE_PATH, "" + i, quality);
        _totalLength += l;
      }
      setTotalLength(_totalLength);

      const arrayBuffer = [];
      for (let j = 0; j <= Conf.VIDEO_TEXTURE_NUM - 1; j++) {
        arrayBuffer[j] = await stream(BASE_PATH, "" + j, quality);
      }
      setVideoBuffer(arrayBuffer);
    })();
  };

  const requestRef = useRef<any>();
  const tick = useCallback(() => {
    requestRef.current = requestAnimationFrame(tick);
    setCurrentLength(_contentLength);
    const p = (_contentLength / _totalLength) * 100;
    setPercent(p);
    if (p == 100) {
      // setTimeoutは保険
      window.setTimeout(() => {
        setIsLoaded(true);
      }, 1000);
    }
  }, [setCurrentLength]);
  useEffect(() => {
    requestRef.current = requestAnimationFrame(tick);
    return () => cancelAnimationFrame(requestRef.current);
  }, [tick]);

  return isLoadStart ? (
    <Root>
      <Info>
        <p className="percent">
          {("000" + ~~percent).slice(-3)}
          <span>%</span>
        </p>
        <div className="bar">
          <span
            style={{
              width: percent + "%",
            }}
          ></span>
        </div>
        <div className="data">
          {currentLength} / {totalLength}
        </div>
      </Info>
    </Root>
  ) : (
    <Root>
      <Header>
        <h1>nocebo - [ prs ] studio rehearsal</h1>
        <p>バンドの練習風景撮ったのをwebgl(three.js)でGPGPU使いつつcurlnoiseのparticleで遊んでみました</p>
        <Share>
          <a
            href="//twitter.com/share?text=nocebo - [ prs ] studio rehearsal&amp;url=https://nocebo.jp/band/prs/public/&amp;hashtags=webgl,threejs"
            title="Tweet"
            target="_blank"
            rel="nofollow noopener"
          >
            SHARE
            <FontAwesomeIcon icon={["fab", "twitter"]} />
          </a>
        </Share>
      </Header>
      <Select>
        <h4>QUALITY SELECT</h4>
        <nav>
          <button
            onClick={() => {
              loadStart("NORMAL");
            }}
          >
            720P
          </button>
          <button
            onClick={() => {
              loadStart("HIGH");
            }}
          >
            1080P
          </button>
        </nav>
        <div className="url">
          <h5>URL</h5>
          <a href="https://nocebo.jp/band/prs/public/" target="_blank">
            https://nocebo.jp/band/prs/public/
          </a>
        </div>
        <div className="link">
          <h5>LINKS</h5>
          <a href="https://nocebo.jp/" target="_blank">
            nocebo.jp
          </a>
          <a href="https://nocebo.jp/band/" target="_blank">
            band
          </a>
          <a href="https://youtu.be/tD1X_cUVVrY" target="_blank">
            youtube
          </a>
          <a href="https://twitter.com/nocebojp" target="_blank">
            twitter
          </a>
          <a href="https://bitbucket.org/honda-nocebo/prs/src/master/" target="_blank">
            git
          </a>
          <a href="https://nocebo.jp/post-3924/" target="_blank">
            explain
          </a>
        </div>
      </Select>
    </Root>
  );
};

export default React.memo(StreamLoader);

const Root = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 10000;
  left: 0;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  button {
    border: 0px;
    font-size: 40px;
    padding: 5px 10px;
    border: 0px;
    background-color: #000;
    color: #fff;
    cursor: pointer;
  }
`;
const Header = styled.header`
  position: fixed;
  top: 40px;
  left: 40px;
  h1 {
    font-size: 36px;
  }
  p {
    margin-top: 30px;
    font-size: 14px;
  }
`;
const Share = styled.div`
  position: fixed;
  right: 40px;
  top: 40px;
  a {
    color: #fff;
    border-radius: 2px;
    text-decoration: none;
    font-size: 20px;
  }
  svg {
    display: inline-block;
    margin-left: 10px;
  }
`;

const Select = styled.div`
  text-align: center;
  h4 {
    font-size: 36px;
  }
  nav {
    display: flex;
    justify-content: space-between;
    margin-top: 40px;
    margin-bottom: 60px;
    button {
      margin: 0 20px;
      background-color: #fff;
      color: #000;
      min-width: 260px;
      padding: 10px;
      border-radius: 5px;
      padding-bottom: 8px;
    }
  }
  .link,
  .url {
    margin-top: 15px;
    h5 {
      font-size: 26px;
      margin-bottom: 15px;
    }
    a {
      display: inline-block;
      color: #fff;
      text-decoration: none;
      margin-bottom: 20px;
      &::after {
        content: "/";
        display: inline-block;
        margin: 0 10px;
      }
      &:last-child {
        &::after {
          display: none;
        }
      }
    }
  }
`;
const Info = styled.div`
  text-align: center;
  .percent {
    font-size: 50px;
    span {
      margin-left: 10px;
      font-size: 30px;
    }
  }
  .bar {
    margin-top: 10px;
    width: 500px;
    height: 20px;
    border: 1px solid #fff;
    border-radius: 10px;
    position: relative;
    overflow: hidden;
    span {
      position: absolute;
      /* /* top: 1px; */
      left: 0px;
      display: inline-block;
      width: 50%;
      height: 20px;
      background-color: rgba(255, 255, 255, 0.8);
      /* border-radius: 10px; */
      /* border: 1px solid #000; */
      box-sizing: border-box;
    }
  }
  .data {
    margin-top: 10px;
    font-size: 10px;
    text-align: right;
  }
`;
