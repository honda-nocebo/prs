// =======================================
// canvasを一コマづつjpegキャプチャとって
// 手動でffmpegで連結する。
// MediaStreamは一応動いたけどコマ落ちするので一旦無し
// 公開サーバーではいらないと思うのでdevelopmentだけ
// =======================================

import React, { useContext, useCallback } from "react";
import styled from "styled-components";
import { updateValue } from "@/data";
import { ResourceContext } from "@/index";

const APP_URL = process.env.APP_URL;

let currentTime = 0;
let currentFrame = 0;

const Recorder: React.FC<any> = (props) => {
  //   console.log("Recorder");
  const { videoRef } = props;
  const [resource, setResource] = useContext(ResourceContext);
  const { canvas } = resource;

  const renderingStart = useCallback(() => {
    console.log(canvas);
    if (!canvas) return;
    currentTime = 0;
    rendering();
  }, [videoRef, setResource, resource]);
  const rendering = useCallback(() => {
    currentFrame += 1;
    const per = ((currentTime / updateValue.duration) * 100).toFixed(2);
    console.log(per + "%");
    if (currentTime > updateValue.duration) return;
    (async () => {
      update();
      await output(currentFrame);
      rendering();
    })();
  }, [videoRef, resource]);

  const update = useCallback(() => {
    currentTime += 1 / 30; // 30FPS
    videoRef.forEach((video: any) => {
      video.current.currentTime = currentTime;
    });
    updateValue.renderState = true;
    // ここでthree側をコマ送りするようにしている
  }, [videoRef, resource]);
  // ----------------------------
  const screenshot = useCallback(() => {
    canvas.getContext("experimental-webgl", {
      preserveDrawingBuffer: true,
    });
    const dataUrl = canvas.toDataURL("image/jpeg");
    const w: any = window.open("about:blank");
    w.document.write("<img src='" + dataUrl + "'/>");
  }, [videoRef, resource]);
  // ----------------------------
  const output = async (currentFrame: number) => {
    canvas.getContext("experimental-webgl", {
      preserveDrawingBuffer: true,
    });
    const base64 = canvas.toDataURL("image/jpeg", 0.5).replace(new RegExp("data:image/jpeg;base64,"), "");
    const formData = new FormData();
    formData.append("base64", base64);
    formData.append("currentFrame", "" + currentFrame);
    const response = await fetch(APP_URL + "/api/generator/", {
      method: "POST",
      body: formData,
    });
    if (!response.ok) return;
    const path = await response.json();
    return new Promise((resolve) => {
      window.setTimeout(() => {
        resolve(path);
      }, 1000);
    });
  };

  return (
    <Root className="Recorder">
      <button onClick={renderingStart}>rendering</button>
      <button onClick={screenshot}>screenshot</button>
    </Root>
  );
};

export default React.memo(Recorder);

const Root = styled.div`
  position: fixed;
  bottom: 30px;
  right: 30px;
  z-index: 20000;

  button {
    cursor: pointer;
    border: 0px solid #fff;
    margin: 0 10px;
    background-color: rgba(255, 255, 255, 0.9);
    color: #000;
    padding: 10px 15px;
    border-radius: 10px;
    font-size: 16px;
  }
`;

// ===========================================
// MediaStream動いたけどコマ落ちするので
// 一コマづつjpegキャプチャとってffmpegで連結する方向で

// const [href, setHref] = useState<any>();
//   const [download, setDownload] = useState<any>();

// const recorder = useMemo(() => {
//     if (!canvas) return;

//     const audioStream = audio.mediaStreamDest.stream;
//     const canvasStream = canvas.captureStream();

//     const mediaStream = new MediaStream();
//     [canvasStream, audioStream].forEach((stream) => {
//       stream.getTracks().forEach((track: any) => mediaStream.addTrack(track));
//     });

//     const recorder = new MediaRecorder(mediaStream, {
//       audioBitsPerSecond: 128000,
//       videoBitsPerSecond: 12800000,
//       mimeType: "video/webm;codecs=vp9",
//     });
//     recorder.ondataavailable = function (e) {
//       var videoBlob = new Blob([e.data], { type: e.data.type });
//       const blobUrl = window.URL.createObjectURL(videoBlob);
//       setDownload("movie.webm");
//       setHref(blobUrl);
//     };

//     return recorder;
//   }, [canvas, audio]);

//   const recorderStart = useCallback(() => {
//     console.log("start");
//     recorder!.start();
//   }, [recorder]);
//   const recorderStop = useCallback(() => {
//     console.log("stop");
//     recorder!.stop();
//   }, [recorder]);
//   // ----------------------------
