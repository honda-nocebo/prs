// =======================================
// WEB AUDIO API周り
// fftでなんか同期しようと思ったけど
// 現状ただグラフとか描画してるだけ
// =======================================

import React, { useEffect, useCallback, useRef, useState, useContext } from "react";
import { ResourceContext } from "@/index";
import styled from "styled-components";

const CV_WIDTH = 300;
const CV_HEIGHT = 150;

declare global {
  interface Window {
    webkitAudioContext: typeof AudioContext;
  }
}
const Audio: React.FC<any> = (props) => {
  const [resource, setResource] = useContext(ResourceContext);
  const [audio, setAudio] = useState<any>();
  const videoRef = props.videoRef[1]; // audioはvideoBから
  const [isData, setIsData] = useState(false);
  const timeRef = useRef();
  const frequencyRef = useRef();
  const levelRef = useRef();

  const [levelCanvasCtx, setLevelCanvasCtx] = useState<CanvasRenderingContext2D>();
  const [timeCanvasCtx, setTimeCanvasCtx] = useState<CanvasRenderingContext2D>();
  const [frequencyCanvasCtx, setFrequencyCanvasCtx] = useState<CanvasRenderingContext2D>();

  const setLevelRef = useCallback(
    (node) => {
      levelRef.current = node;
      setLevelCanvasCtx(node.getContext("2d"));
    },
    [levelRef]
  );
  const setTimeRef = useCallback(
    (node) => {
      timeRef.current = node;
      setTimeCanvasCtx(node.getContext("2d"));
    },
    [timeRef]
  );
  const setFrequencyRef = useCallback(
    (node) => {
      frequencyRef.current = node;
      setFrequencyCanvasCtx(node.getContext("2d"));
    },
    [frequencyRef]
  );

  useEffect(() => {
    if (timeRef || frequencyRef) {
      const _AudioContext = window.AudioContext || window.webkitAudioContext;
      const audioCtx: AudioContext = new _AudioContext();
      const source = audioCtx.createMediaElementSource(videoRef.current);

      const gainNode = audioCtx.createGain();
      gainNode.gain.value = 1.0;

      const analyserNode: AnalyserNode = audioCtx.createAnalyser();
      analyserNode.fftSize = 512;
      // frequencyBinCountはナイキストレートでfftSizeの半分
      analyserNode.smoothingTimeConstant = 0.6;

      const levelBuffer: Float32Array = new Float32Array(analyserNode.fftSize);
      const timesData: Uint8Array = new Uint8Array(analyserNode.frequencyBinCount);
      const freqsData: Uint8Array = new Uint8Array(analyserNode.frequencyBinCount);

      const processor = audioCtx.createScriptProcessor(1024, 1, 1);
      source.connect(processor);
      // スクリプトプロセッサ。はいらないかも

      const mediaStreamDest = audioCtx.createMediaStreamDestination();
      source.connect(mediaStreamDest);
      //   mediaStream(録音用)

      source.connect(gainNode);
      gainNode.connect(analyserNode);
      analyserNode.connect(audioCtx.destination);

      // なんか下のuseEffectと合わせて
      // こうしないとresourceに反映されない
      // 追ってuseContext確認
      window.setTimeout(() => {
        setAudio({
          audioCtx: audioCtx,
          processor: processor,
          analyserNode: analyserNode,
          levelBuffer: levelBuffer,
          timesData: timesData,
          freqsData: freqsData,
          mediaStreamDest: mediaStreamDest,
        });
        setIsData(true);
      }, 500);
    }
  }, [levelRef, timeRef, frequencyRef, videoRef, setAudio]);
  // ----------------------
  useEffect(() => {
    setResource({
      ...resource,
      ["audio"]: audio,
    });
  }, [audio, setResource]);
  // ----------------------
  // draw
  // ----------------------
  const levelDraw = useCallback(() => {
    // console.log("levelDraw");
    audio.analyserNode.getFloatTimeDomainData(audio.levelBuffer);
    // ---------------------
    // // Compute average power over the interval.
    // for (let i = 0; i < sampleBuffer.length; i++) {
    //   sumOfSquares += sampleBuffer[i] ** 2;
    // }
    // const avgPowerDecibels = 10 * Math.log10(sumOfSquares / sampleBuffer.length);
    // ---------------------
    // Compute peak instantaneous power over the interval.
    let peak = 0;
    let sumOfSquares = 0;
    for (let i = 0; i < audio.levelBuffer.length; i++) {
      const power = audio.levelBuffer[i] ** 2;
      peak = Math.max(power, peak);
      sumOfSquares += power;
    }
    const peakDecibels = 10 * Math.log10(peak);
    // ピークデシベル
    const avgDecibels = 10 * Math.log10(sumOfSquares / audio.levelBuffer.length);
    // デシベル平均値
    const peakLinear = Math.pow(10, peakDecibels / 20);
    const avgLinear = Math.pow(10, avgDecibels / 20);
    // リニア値

    levelCanvasCtx!.clearRect(0, 0, 1000, 100);
    const grd = levelCanvasCtx!.createLinearGradient(0, 0, CV_WIDTH, 0);

    grd.addColorStop(0.0, "lime");
    grd.addColorStop(0.7, "yellow");
    grd.addColorStop(1.0, "red");
    levelCanvasCtx!.fillStyle = grd;
    levelCanvasCtx!.fillRect(0, 0, CV_WIDTH * peakLinear * 2.0, 50);
  }, [audio, levelCanvasCtx, resource]);

  const timeDraw = useCallback(() => {
    // console.log("timeDraw");
    audio.analyserNode!.getByteTimeDomainData(audio.timesData!);
    timeCanvasCtx!.clearRect(0, 0, CV_WIDTH, CV_HEIGHT);
    timeCanvasCtx!.strokeStyle = "rgb(255, 255, 255)";
    timeCanvasCtx!.beginPath();
    for (var i = 0; i < audio.analyserNode!.frequencyBinCount; ++i) {
      var x = (i / audio.analyserNode!.frequencyBinCount) * CV_WIDTH;
      var val = 1.0 - audio.timesData![i] / 255; //   0.0 ~ 1.0の数値に
      val = (val - 0.5) * 1.0 + 0.5; //スケーリング
      var y = val * CV_HEIGHT;

      if (i === 0) {
        timeCanvasCtx!.moveTo(x, y);
      } else {
        timeCanvasCtx!.lineTo(x, y);
      }
    }
    timeCanvasCtx!.stroke();
  }, [audio, timeCanvasCtx, resource]);

  const frequencyDraw = useCallback(() => {
    // console.log("frequencyDraw");
    audio.analyserNode!.getByteFrequencyData(audio.freqsData!);
    frequencyCanvasCtx!.clearRect(0, 0, CV_WIDTH, CV_HEIGHT);
    const frequencyArea = ~~(audio.analyserNode!.frequencyBinCount * 1.0);
    frequencyCanvasCtx!.strokeStyle = "rgb(255, 255, 255)";
    frequencyCanvasCtx!.beginPath();
    for (var i = 0; i < frequencyArea; ++i) {
      var x = (i / frequencyArea) * CV_WIDTH;
      var y = (1 - audio.freqsData![i] / 255) * CV_HEIGHT;

      if (i === 0) {
        frequencyCanvasCtx!.moveTo(x, y);
      } else {
        frequencyCanvasCtx!.lineTo(x, y);
      }
    }
    frequencyCanvasCtx!.stroke();
  }, [audio, frequencyCanvasCtx, resource]);

  // ----------------------
  // update処理
  // ----------------------
  const tick = useCallback(() => {
    requestAnimationFrame(tick);
    if (!isData) return;
    if (!resource.isDataShow) return;
    levelDraw();
    timeDraw();
    frequencyDraw();
  }, [audio, isData, resource, frequencyDraw, timeDraw]);
  useEffect(() => {
    const _tick = requestAnimationFrame(tick);
    return () => cancelAnimationFrame(_tick);
  }, [tick, resource]);

  // ----------------------
  // render
  // ----------------------
  return (
    <Root className="Audio" style={{ display: resource.isDataShow ? "block" : "none" }}>
      <LevelCanvas ref={setLevelRef} width={CV_WIDTH} height={50} />
      <TimeCanvas ref={setTimeRef} width={CV_WIDTH} height={CV_HEIGHT} />
      <FrequencyCanvas ref={setFrequencyRef} width={CV_WIDTH} height={CV_HEIGHT} />
    </Root>
  );
};

export default Audio;

const Root = styled.div`
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
`;
const LevelCanvas = styled.canvas`
  width: 300px;
  height: 50px;
  position: fixed;

  top: 140px;
  left: 20px;
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 10px;
`;

const TimeCanvas = styled.canvas`
  position: fixed;
  width: 300px;
  height: 150px;
  top: 205px;
  left: 20px;
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 10px;
`;
const FrequencyCanvas = styled.canvas`
  width: 300px;
  height: 150px;
  position: fixed;
  top: 372px;
  left: 20px;
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 10px;
`;
