// =======================================
// ナビゲーションドラッグの処理とか
// =======================================
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import NaV from "@/ui/nav";

class Point {
  x: number;
  y: number;
  constructor(x: number = 0, y: number = 0) {
    this.x = x || 0;
    this.y = y || 0;
  }
  static distance(a: Point, b: Point) {
    const dx = a.x - b.x;
    const dy = a.y - b.y;
    return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
  }
  static interpolate(p1: Point, p2: Point, t: number) {
    let x = p1.x * (1 - t) + p2.x * t;
    let y = p1.y * (1 - t) + p2.y * t;
    return new Point(x, y);
  }
  add(p: Point) {
    this.x += p.x;
    this.y += p.y;
    return this;
  }
  subtract(p: Point) {
    this.x -= p.x;
    this.y -= p.y;
    return this;
  }
}
// ========================================
const disableTouch = (e: TouchEvent) => {
  e.preventDefault();
};
const touchPosition = (e: any, state: string = "client") => {
  if (state == "client") {
    return new Point(e.nativeEvent.clientX || 0, e.nativeEvent.clientY || 0);
  }
  if (state == "offset") {
    return new Point(e.nativeEvent.offsetX || 0, e.nativeEvent.offsetY || 0);
  }
};
// ========================================

const UI: React.FC<any> = (props) => {
  const { videoRef } = props;
  //   console.log(videoState);
  const [count, setCount] = useState(0);
  const [isDown, setIsDown] = React.useState(false);
  const [visible, setVisible] = React.useState<boolean>(true);
  const [startPoints, setStartPoints] = React.useState<any>([new Point(), new Point()]);
  const [tempPoints, setTempPoints] = React.useState<any>([new Point(), new Point()]);
  const [data, setData] = React.useState({
    x: window.innerWidth * 0.5 - 190,
    y: window.innerHeight - 180,
    scale: 1,
    rotate: 0,
  });

  // ----------------------
  React.useEffect(() => {
    document.addEventListener("touchstart", disableTouch, {
      passive: false,
    });
    return () => {
      document.removeEventListener("touchstart", disableTouch);
    };
  }, []);
  // ----------------------
  useEffect(() => {
    if (count > 1) return;
    const timerId = setInterval(() => {
      const c = count + 1;
      if (c == 1) setVisible(false);
      setCount(c);
    }, 100);
    return () => clearInterval(timerId);
  }, [count]);
  // ----------------------

  const onDown = React.useCallback(
    (e: React.TouchEvent | React.MouseEvent) => {
      //   e.preventDefault();
      setVisible(true);

      const start: any = touchPosition(e);
      const temp: any = new Point(data.x, data.y);

      setStartPoints(start);
      setTempPoints(temp);
    },
    [isDown, data]
  );

  const onMove = React.useCallback(
    (e: React.TouchEvent | React.MouseEvent) => {
      //   e.preventDefault();
      setVisible(true);
      setCount(0);

      if (!isDown) return;
      const current: any = touchPosition(e);
      const diff = new Point(current.x, current.y).subtract(startPoints);

      data.x = tempPoints.x + diff.x;
      data.y = tempPoints.y + diff.y;

      setData({ ...data });
    },
    [isDown, data]
  );

  const onUp = React.useCallback(
    (e: any) => {
      e.preventDefault();
      setIsDown(false);
      setCount(0);
    },
    [isDown]
  );

  const transformStyle = React.useMemo(() => {
    return {
      transform: `translate(${data.x}px ,${data.y}px)`,
    };
  }, [data]);

  return (
    <Root className={"UI" + " " + (visible ? "active" : "")} onMouseDown={onDown} onMouseMove={onMove} onMouseUp={onUp}>
      <div
        className={"inner" + " " + (visible ? "active" : "")}
        onMouseDown={() => {
          setIsDown(true);
        }}
        style={transformStyle}
      >
        <NaV videoRef={videoRef} />
      </div>
    </Root>
  );
};

export default UI;

const Root = styled.div`
  position: fixed;
  z-index: 10000;
  width: 100%;
  height: 100%;
  .inner {
    opacity: 0;
    transition: opacity 0.2s linear 1.5s;
    width: 380px;
    &.active {
      opacity: 1;
      transition: opacity 0.2s linear 0s;
    }
  }
`;
