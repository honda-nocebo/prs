// =======================================
// ビデオの制御とかデータ表示制御
// =======================================
import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ResourceContext } from "@/index";

import { updateValue } from "@/data";

const requestFullScreen = (elm: any) => {
  //   console.log("full");
  if (!!elm.requestFullScreen) {
    elm.requestFullScreen();
  } else if (!!elm.webkitRequestFullScreen) {
    elm.webkitRequestFullScreen();
  } else if (!!elm.webkitEnterFullscreen) {
    elm.webkitEnterFullscreen();
  } else if (!!elm.mozRequestFullScreen) {
    elm.mozRequestFullScreen();
  } else if (!!elm.msRequestFullscreen) {
    elm.msRequestFullscreen();
  }
};

const exitFullScreen = (elm: any) => {
  //   console.log("exit");
  if (!!elm.exitFullscreen) {
    elm.exitFullscreen();
    console.log("exit");
  } else if (!!elm.cancelFullScreen) {
    elm.cancelFullScreen();
  } else if (!!elm.mozCancelFullScreen) {
    elm.mozCancelFullScreen();
  } else if (!!elm.webkitCancelFullScreen) {
    elm.webkitCancelFullScreen();
  } else if (!!elm.msExitFullscreen) {
    elm.msExitFullscreen();
  }
};

const UI: React.FC<any> = (props) => {
  const { videoRef } = props;
  const [isDown, setIsDown] = useState(false);
  const [fullState, setFullState] = useState(false);
  const [resource, setResource] = useContext(ResourceContext);

  const [range, setRange] = useState<any>(0);
  useEffect(() => {
    const intervalId = setInterval(() => {
      if (isDown) return;
      if (resource.videoState == "pause") return;
      let r = updateValue.videoCurrentTime / updateValue.duration;
      if (Number.isNaN(r)) r = 0;
      setRange(r * 100);
    });
    return () => clearInterval(intervalId);
  }, [setRange, isDown, resource]);
  // ----------------------

  return (
    <Root className="UI">
      <div
        className="range"
        onMouseDown={(e: any) => {
          e.stopPropagation();
          setIsDown(true);
        }}
        onMouseUp={(e: any) => {
          e.stopPropagation();
          setIsDown(false);
        }}
      >
        <input
          type="range"
          value={range}
          onChange={(e: any) => {
            videoRef.forEach((video: any) => {
              video.current.currentTime = (e.target.value / 100) * video.current.duration;
            });
            setRange(e.target.value);
          }}
        ></input>
      </div>
      <button
        onClick={() => {
          if (resource.videoState == "play") {
            videoRef.forEach((video: any) => {
              video.current.pause();
            });
            setResource({ ...resource, ["videoState"]: "pause" });
            updateValue.videoState = "puase";
          } else {
            videoRef.forEach((video: any) => {
              video.current.play();
            });
            setResource({ ...resource, ["videoState"]: "play" });
            updateValue.videoState = "play";
          }
        }}
      >
        {resource.videoState == "play" ? (
          <FontAwesomeIcon icon={["fas", "pause"]} />
        ) : (
          <FontAwesomeIcon className="play" icon={["fas", "play"]} />
        )}
      </button>
      <button
        className="data"
        onClick={() => {
          setResource({
            ...resource,
            ["isDataShow"]: !resource.isDataShow,
          });
        }}
      >
        {resource.isDataShow ? <FontAwesomeIcon icon={["fas", "toggle-on"]} /> : <FontAwesomeIcon icon={["fas", "toggle-off"]} />}
      </button>

      <button
        onClick={() => {
          if (fullState) {
            exitFullScreen(document);
            setFullState(false);
          } else {
            requestFullScreen(document.documentElement);
            setFullState(true);
          }
        }}
      >
        {fullState ? <FontAwesomeIcon icon={["fas", "compress"]} /> : <FontAwesomeIcon icon={["fas", "expand"]} />}
      </button>
    </Root>
  );
};

export default UI;

const Root = styled.nav`
  padding: 0 20px;

  background-color: #333;
  opacity: 0.8;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 15px 20px;
  border-radius: 10px;
  flex-wrap: wrap;

  button {
    margin: 0 8px;
    color: #000;
    width: 50px;
    height: 50px;
    border-radius: 50px;
    appearance: none;
    border: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    font-size: 20px;
    padding: 10px;
    box-sizing: border-box;
    cursor: pointer;
    &.data {
      font-size: 25px;
    }
    .play {
      display: inline-block;
      padding-left: 6px;
    }
  }

  .range {
    width: 100%;
    margin-bottom: 15px;
    /* background-color: #000; */
    input[type="range"] {
      display: inline-block;
      position: relative;
      width: 100%;
    }
  }
`;
