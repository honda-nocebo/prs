// =======================================
// データ確認表示用
// あとアップデート値(updateValue)の
// tick処理もここでやっちゃってる
// =======================================

import React, { useEffect, useCallback, useContext, useState, useRef } from "react";
import { ResourceContext, Conf } from "@/index";
import styled from "styled-components";

const BPM_NUM = 1.565;
const START_TIME = 5.0;

export const updateValue: any = {
  videoState: "pause",
  videoCurrentTime: 0,
  videoTempTime: 0,
  duration: 0,

  renderState: false,

  bar: 0,
  beat: 0,
  beatState: false,
  barState: false,
  lim01State: false,
  lim02State: false,
  kick01State: false,
  kick02State: false,
  copyTexState01: false,
  copyTexState02: false,
  copyTexState03: false,
  copyTexState04: false,

  videoId: 0,
  range: 1.0,
  originX: 0.0,
  originY: 0.0,
  zoom: 0.0,

  seed: Math.random() * 1.0,
  copyTex: false,
  bgOpacity: 1.0,
  referenceColor: 1.0,
  referenceVelocity: 1.0,
  delta: 0.02,
  lim: 0.0,
  blurStrength: 0.0,
  flow01: 1.0,
  flow02: 1.0,
  flow03: 1.0,
  flow04: 1.0,
  flow05: 1.0,
  prev: {},
};
updateValue.prev = updateValue;

const swicher = () => {
  // -------------
  if (updateValue.bar == 28) return;
  // -------------
  // 前回のと重複しないように
  let nextId = Math.floor(Math.random() * Conf.VIDEO_TEXTURE_NUM);
  do {
    nextId = Math.floor(Math.random() * Conf.VIDEO_TEXTURE_NUM);
  } while (updateValue.videoId == nextId);
  updateValue.tempId = updateValue.videoId;
  updateValue.prev = { ...updateValue };
  updateValue.videoId = nextId;
  // ------
  // 拡大位置・スケール
  const range = Math.random() * 0.4 + 0.3;
  const originX = Math.random() * (1.0 - range);
  const originY = Math.random() * (1.0 - range);
  updateValue.range = range;
  updateValue.originX = originX;
  updateValue.originY = originY;
  updateValue.zoom = 0.085;
};

const Data: React.FC<any> = (props) => {
  const [resource] = useContext(ResourceContext);
  const videoRef = props.videoRef[1];
  // ----------------------
  // 表示用
  // ----------------------
  const [bar, setBar] = useState(0);
  const [time, setTime] = useState<number>(0);
  const [millisecond, setMillisecond] = useState<number>(0);
  //   -----------------------
  //   update
  //   -----------------------
  const requestRef = useRef<any>();

  const clear = () => {
    updateValue.lim01State = false;
    updateValue.lim02State = false;
    updateValue.kick01State = false;
    updateValue.kick02State = false;
    updateValue.copyTexState01 = false;
    updateValue.copyTexState02 = false;
    updateValue.copyTexState03 = false;
    updateValue.copyTexState04 = false;
    updateValue.bgOpacity = 1.0;
    updateValue.referenceColor = 1.0;
    updateValue.referenceVelocity = 1.0;
    updateValue.blurStrength = 0.0;
  };

  const perticleMotion = (barNum: number, beatNum: number) => {
    //   パーティクル止めるやつ
    if (updateValue.videoTempTime == updateValue.videoCurrentTime) return;

    // 再生時と書き出し時で見え方違うので調整

    // 再生時
    const n = 0.0;
    const rc = 0.06;
    const bo = 0.06;

    // 書き出し時
    // const n = 0.0;
    // const rc = 0.09;
    // const bo = 0.09;

    // // 少し前のバッファ保存
    if (beatNum > n - 0.02 && !updateValue.copyTexState01) {
      updateValue.copyTexState01 = true;
      updateValue.copyTex = true;
    }
    // 止めるタイミング
    if (beatNum > n && !updateValue.copyTexState02) {
      updateValue.copyTexState02 = true;
      // 発生確率ランダム
      // if (Math.random() >= 0.35) return;
      //   console.log("perticleMotion");
      updateValue.bgOpacity = 0.0;
      updateValue.referenceColor = 0.0;
      updateValue.referenceVelocity = 0.0;
    }
    if (beatNum > 0.1) {
      updateValue.referenceColor += rc;
    }
    // 戻すタイミング
    if (beatNum > 0.23 && !updateValue.copyTexState03) {
      updateValue.copyTexState03 = true;
      updateValue.referenceVelocity = 1.0;
    }
    if (beatNum > 0.23 && beatNum < 1.4) {
      updateValue.bgOpacity += bo;
    }
    if (beatNum > 0.5) {
    }
    // リム2
    if (barNum >= 36) {
      if (!(barNum >= 68 && barNum < 76)) {
        if (beatNum > 1.4) {
          updateValue.bgOpacity = 0.0;
        }
      }
    }
  };
  const tick = useCallback(() => {
    requestRef.current = requestAnimationFrame(tick);
    // onTimeUpdateだとchromeで？間隔が遅いので 自前tick処理でcurrentTime設定
    // フレーム毎の変化値はcompute.tsxでやってる

    const time = videoRef.current.currentTime;
    updateValue.videoTempTime = updateValue.videoCurrentTime;
    updateValue.videoCurrentTime = time;

    setTime(updateValue.videoCurrentTime);
    setMillisecond(Math.abs((updateValue.videoCurrentTime - START_TIME) % BPM_NUM));
    // 表示確認用

    //   if (updateValue.videoState != "play") return;
    if (time < START_TIME) return;

    // bar 頭 // beat 小節
    const beatNum = (updateValue.videoCurrentTime - START_TIME) % BPM_NUM;
    const barNum = Math.floor((updateValue.videoCurrentTime - START_TIME) / BPM_NUM);

    // 終盤まで再生で一旦クリア
    if (barNum >= 108) {
      updateValue.barState = false;
      clear();
      return;
    }

    // 小節頭
    if (Math.floor(beatNum) == 0 && !updateValue.barState) {
      // console.log("bar!!");
      updateValue.barState = true;
      updateValue.seed = Math.random() * 1.0;
      clear();
      setBar(barNum);
      swicher();
    }

    //2小節目
    if (Math.floor(beatNum) == 1) {
      // console.log("second!!");
      updateValue.barState = false;
    }

    // キック2
    if (beatNum > 1.2 && !updateValue.kick02State) {
      // console.log("kick02");
      updateValue.kick02State = true;
      updateValue.blurStrength = Math.random() * 0.2 + 0.1;
      if (barNum >= 84) {
        swicher();
      }
    }

    // リム1
    if (barNum >= 36) {
      if (!(barNum >= 68 && barNum < 76)) {
        if (beatNum > 0.5 && !updateValue.lim01State) {
          updateValue.lim01State = true;
          updateValue.lim = 1.0;
          updateValue.flow02 = 1.0;
          updateValue.seed = Math.random() * 1.0;
        }
      }
    }

    // リム2
    if (barNum >= 36) {
      if (!(barNum >= 68 && barNum < 76)) {
        if (beatNum > 1.4 && !updateValue.lim02State) {
          updateValue.lim02State = true;
          updateValue.lim = 1.0;
          // updateValue.flow = 2.0;
          // updateValue.flow02 = 1.0;
          updateValue.speedToggle = 1.0;
        }
      }
    }

    perticleMotion(barNum, beatNum);
  }, [window, resource, setTime, setMillisecond]);
  useEffect(() => {
    requestRef.current = requestAnimationFrame(tick);
    return () => cancelAnimationFrame(requestRef.current);
  }, [tick]);

  // ----------------------
  // render
  // ----------------------
  //   return <></>;
  return resource.isDataShow ? (
    <Root>
      <p>videoState: {resource.videoState} </p>
      <p>currentTime: {time} </p>
      <p>bar: {bar}</p>
      <p>Millisecond: {millisecond.toFixed(3)} </p>
    </Root>
  ) : null;
};

export default Data;

const Root = styled.div`
  position: fixed;
  z-index: 1000;
  top: 20px;
  left: 20px;
  background-color: rgba(0, 0, 0, 0.5);
  color: #fff;
  width: 300px;

  padding: 10px;
  opacity: 0.5;
  line-height: 1.4;
  border-radius: 10px;
  box-sizing: border-box;
`;
