// =======================================
// effectComposerはここで
// =======================================

import React, { useRef, useEffect } from "react";
import { useFrame, useThree } from "react-three-fiber";
import { extend } from "react-three-fiber";

import * as THREE from "three";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass";
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass";
import { FXAAShader } from "three/examples/jsm/shaders/FXAAShader";
import { SSAOPass } from "three/examples/jsm/postprocessing/SSAOPass";
import { UnrealBloomPass } from "three/examples/jsm/postprocessing/UnrealBloomPass";

extend({ EffectComposer, RenderPass, ShaderPass, FXAAShader, SSAOPass, UnrealBloomPass });

// インターフェイスIntrinsicElementsにorbitControls の定義を追加
// typescriptめんどい
declare global {
  namespace JSX {
    interface IntrinsicElements {
      effectComposer: any;
      renderPass: any;
      shaderPass: any;
      unrealBloomPass: any;
    }
  }
}

const Effect: React.FC<any> = (props) => {
  const { gl, scene, camera, size } = useThree();
  const composer: any = useRef();

  useFrame(() => {
    if (!composer.current) return;
    composer.current.render();
  }, 30);
  useEffect(() => {
    composer.current.setSize(size.width, size.height), [size];
  });

  const bloom: any = {
    resolution: new THREE.Vector2(size.width, size.height),
    strength: 0.5,
    radius: 0.01,
    threshold: 0.3,
  };

  return (
    <effectComposer ref={composer} args={[gl]}>
      <renderPass attachArray="passes" scene={scene} camera={camera} />
      <unrealBloomPass attachArray="passes" args={[bloom.resolution, bloom.strength, bloom.radius, bloom.threshold]} />
    </effectComposer>
  );
};

export default Effect;
