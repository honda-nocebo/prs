// =======================================
// threeの呼び出し元
// =======================================
import React, { useRef, useState, useCallback, useEffect, useContext } from "react";
import { Vector3, PerspectiveCamera } from "three";
import { Canvas, useThree, useFrame } from "react-three-fiber";
import styled from "styled-components";
import Compute from "@/gl/compute";
import Scene from "@/gl/scene";
import Particles from "@/gl/particles/index";
import Effect from "@/gl/effect/";
import { Conf } from "@/index";
import { ResourceContext } from "@/index";

const GL: React.FC<any> = (props) => {
  //   console.log("GL");
  const [resource, setResource] = useContext(ResourceContext);
  // const { gl } = useThree(); // glってのがrenderer
  // gl.setPixelRatio(window.devicePixelRatio);// retinaだとここでテクスチャずれるかも

  const { videoRef } = props;
  const [canvas, setCanvas] = useState<any>();

  const [canvasSize, setCanvasSize] = useState({
    width: Conf.GL_SIZE_FIX ? Conf.TEXTURE_WIDTH : window.innerWidth,
    height: Conf.GL_SIZE_FIX ? Conf.TEXTURE_HEIGHT : window.innerWidth * (9 / 16),
  });
  const resize = useCallback(() => {
    setCanvasSize({
      width: Conf.GL_SIZE_FIX ? Conf.TEXTURE_WIDTH : window.innerWidth,
      height: Conf.GL_SIZE_FIX ? Conf.TEXTURE_HEIGHT : window.innerWidth * (9 / 16),
    });
  }, []);

  useEffect(() => {
    window.addEventListener("resize", resize);
    return () => window.removeEventListener("resize", resize);
  });

  useEffect(() => {
    // なんかこうしないresourceに反映されない
    setResource({
      ...resource,
      ["canvas"]: canvas,
    });
  }, [canvas, setResource]);

  const canvasRef = useRef<HTMLCanvasElement>();
  const setCanvasRef = useCallback(
    (node: any) => {
      if (!node) return;
      setCanvas(node.getElementsByTagName("canvas")[0]);
    },
    [canvasRef]
  );

  const [compute, setCompute] = useState<any>({
    isSet: false,
  });

  return (
    <Root ref={setCanvasRef} className="GL">
      <Canvas gl={{ preserveDrawingBuffer: true }} style={{ width: canvasSize.width, height: canvasSize.height }}>
        <Camera />
        <Compute videoRef={videoRef} compute={compute} setCompute={setCompute} />
        <Scene compute={compute} />
        <Particles compute={compute} />
        <Effect />
      </Canvas>
    </Root>
  );
};
export default GL;

// -------------------------------
const Camera = () => {
  const ref = useRef<PerspectiveCamera>();
  const { setDefaultCamera } = useThree();
  useEffect(() => {
    if (!ref.current) return;
    setDefaultCamera(ref.current);
    ref.current.lookAt(new Vector3(0, 0, 0));
  }, [ref, setDefaultCamera]);

  return <perspectiveCamera position={[0, 0, 10]} args={[45, 16 / 9, 1, 2000]} ref={ref} />;
};

// -------------------------------
const Root = styled.div`
  position: absolute;
  background-color: #fff;
  background-color: #000;
  z-index: 100;
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  canvas {
    width: 100vw;
    height: calc(100vw * 9 / 16);
  }
`;
