// =======================================
// パーティクルの色
// vertexからvaryingで送られてくるの参照
// =======================================
precision mediump float;

const float redScale = 0.298912;
const float greenScale = 0.586611;
const float blueScale = 0.114478;
const vec3 monochromeScale = vec3(redScale, greenScale, blueScale);

vec3 powColor(vec3 c, float p) {
  return vec3(pow(c.r, p), pow(c.g, p), pow(c.b, p));
}

uniform float lim;
uniform float bgOpacity;
uniform float referenceColor;
varying vec4 vColor;

void main() {

  vec4 renderColor = vec4(vColor.rgb, 0.5);
  renderColor = vec4(powColor(renderColor.rgb, 2.0), 1.0);
  vec3 grayColor = vec3(dot(vColor.rgb, monochromeScale));
  grayColor = powColor(grayColor, 1.3);
  vec4 result =
      mix(vec4(grayColor, 1.0), renderColor, max(lim, 1.0 - referenceColor));

  gl_FragColor = result;
}
