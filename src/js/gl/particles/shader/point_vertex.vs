// =======================================
// texturePositionの位置参照して位置更新
// 色もvaryingでテクスチャ参照してfragmentに送る
// =======================================
precision mediump float;

uniform sampler2D texturePosition;
uniform sampler2D textureTempPosition;
uniform sampler2D textureColor;
uniform sampler2D textureTempColor;
uniform float referenceColor;

varying vec4 vColor;

void main() {
  // 位置は固定
  vec3 position = texture2D(texturePosition, uv).xyz;
  vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
  gl_PointSize = 4.0 * (1.0 / -mvPosition.z);
  gl_Position = projectionMatrix * mvPosition;
  //---------------------------
  //   こっちでvaryingする色決める
  float x = (position.x + 8.0) / 16.0;
  float y = (position.y + 4.5) / 9.0;
  //   0.0 ~ 1.0に変換
  x = ((x - 0.5) * (10.2 / -mvPosition.z)) + 0.5;
  y = ((y - 0.5) * (10.2 / -mvPosition.z)) + 0.5;
  // z軸打ち消す

  // (10.0 / -mvPosition.z))の部分は
  // 10.0 ベースに scenceとparticleのmesh.current.position.zの差かな(多分)
  // 適当だけど何となく合ったのでよしとする

  vec2 currentUV = vec2(x, y);
  vec4 texColor = texture2D(textureColor, currentUV);
  //  --------------

  // 静止時用カラー
  vec3 tempPosition = texture2D(textureTempPosition, uv).xyz;
  vec4 tempMvPosition = modelViewMatrix * vec4(tempPosition, 1.0);
  float tx = (tempPosition.x + 8.0) / 16.0;
  float ty = (tempPosition.y + 4.5) / 9.0;
  tx = ((tx - 0.5) * (10.2 / -tempMvPosition.z)) + 0.5;
  ty = ((ty - 0.5) * (10.2 / -tempMvPosition.z)) + 0.5;
  vec2 tempUV = vec2(tx, ty);
  vec4 tempColor = texture2D(textureTempColor, tempUV);
  //---------------------------

  vec4 result = mix(texColor, tempColor, 1.0 - referenceColor);

  vColor = result;
}