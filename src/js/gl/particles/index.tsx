// =======================================
// Particles周り
// 動きは基本compute側で処理したの参照するだけ
// =======================================

import React, { useRef, useMemo, useEffect } from "react";
import * as THREE from "three";
import { useFrame } from "react-three-fiber";
import VertexShader from "@/gl/particles/shader/point_vertex.vs";
import FragmentShader from "@/gl/particles/shader/point_fragment.fs";
import { updateValue } from "@/data";
import { Conf } from "@/index";

const Particles: React.FC<any> = (props) => {
  //   console.log("Particles");
  const material: any = useRef();
  const mesh: any = useRef();
  const { compute } = props;
  // ==================================================================

  // オブジェクトの初期頂点とかUV座標の設定
  const particles = useMemo(() => {
    const vertices = new Float32Array(Conf.PARTICLE_COUNT * 3).fill(0);
    // vertices初期位置は0
    const uv = new Float32Array(Conf.PARTICLE_COUNT * 2);
    for (let i = 0; i < uv.length; i += 2) {
      const indexVertex = i / 2;
      uv[i] = (indexVertex % Conf.TEXTURE_HEIGHT) / Conf.TEXTURE_WIDTH;
      uv[i + 1] = Math.floor(indexVertex / Conf.TEXTURE_WIDTH) / Conf.TEXTURE_HEIGHT;
    }
    // uv座標
    // const colors = new Float32Array(Conf.PARTICLE_COUNT * 3).fill(0);// 色情報
    return {
      vertices: vertices,
      uv: uv,
      //   colors: colors,
      uniforms: {
        time: { value: 0.0 },
        lim: { value: 0.0 },
        bgOpacity: { value: 0.0 },
        referenceColor: { value: 0.0 },
        resolution: { value: new THREE.Vector2(Conf.TEXTURE_WIDTH, Conf.TEXTURE_HEIGHT) },
        texturePosition: { value: null },
        textureColor: { value: null },
        textureTempColor: { value: null },
        textureTempPosition: { value: null },
      },
      VertexShader: VertexShader,
      FragmentShader: FragmentShader,
    };
  }, [Conf.PARTICLE_COUNT]);

  // ==================================================================
  useFrame((frame) => {
    if (material.current) return;
    // ===============
    particles.uniforms.lim.value = updateValue.lim;
    particles.uniforms.bgOpacity.value = updateValue.bgOpacity;
    particles.uniforms.referenceColor.value = updateValue.referenceColor;
    particles.uniforms.texturePosition.value = compute.computeRenderer.getCurrentRenderTarget(compute.texturePosition).texture;
    particles.uniforms.textureColor.value = compute.computeRenderer.getCurrentRenderTarget(compute.textureColor).texture;
    particles.uniforms.textureTempColor.value = compute.computeRenderer.getCurrentRenderTarget(compute.textureTempColor).texture;
    particles.uniforms.textureTempPosition.value = compute.computeRenderer.getCurrentRenderTarget(
      compute.textureTempPosition
    ).texture;
  });
  // ==================================================================
  useEffect(() => {
    if (!mesh.current) return;
    mesh.current.position.z = 0.0;
  }, []);

  return (
    <points ref={mesh}>
      <bufferGeometry attach="geometry">
        <bufferAttribute
          attachObject={["attributes", "position"]}
          //   ref={attrib}
          count={particles.vertices.length / 3} //countはreact-three-fiberで必要っぽい
          array={particles.vertices}
          itemSize={3}
        />
        <bufferAttribute attachObject={["attributes", "uv"]} count={particles.uv.length / 2} array={particles.uv} itemSize={2} />
        {/* <bufferAttribute
          attachObject={["attributes", "colors"]}
          count={particles.colors.length / 3}
          array={particles.colors}
          itemSize={3}
        /> */}
      </bufferGeometry>

      <shaderMaterial
        attach="material"
        uniforms={particles.uniforms}
        vertexShader={particles.VertexShader}
        fragmentShader={particles.FragmentShader}
        transparent={true}
        blending={THREE.AdditiveBlending}
        side={THREE.DoubleSide}
        depthTest={false}
        depthWrite={false}
      />
    </points>
  );
};

export default Particles;
