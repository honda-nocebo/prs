// =======================================
// .glslとかの定義ファイル
// =======================================
declare module "*.vs" {
  const src: string;
  export default src;
}
declare module "*.fs" {
  const src: string;
  export default src;
}
declare module "*.glsl" {
  const src: string;
  export default src;
}
