// =======================================
// ビデオテクスチャモノクロにするやつ
// =======================================
const float redScale = 0.298912;
const float greenScale = 0.586611;
const float blueScale = 0.114478;
const vec3 monochromeScale = vec3(redScale, greenScale, blueScale);

vec3 powColor(vec3 c, float p) {
  return vec3(pow(c.r, p), pow(c.g, p), pow(c.b, p));
}

//  ----------------------------------
//   gaussian ブラー
//  ----------------------------------

vec4 blur13(sampler2D image, vec2 uv, vec2 _resolution, vec2 direction) {
  vec4 color = vec4(0.0);
  vec2 off1 = vec2(1.411764705882353) * direction;
  vec2 off2 = vec2(3.2941176470588234) * direction;
  vec2 off3 = vec2(5.176470588235294) * direction;
  color += texture2D(image, uv) * 0.1964825501511404;
  color += texture2D(image, uv + (off1 / _resolution)) * 0.2969069646728344;
  color += texture2D(image, uv - (off1 / _resolution)) * 0.2969069646728344;
  color += texture2D(image, uv + (off2 / _resolution)) * 0.09447039785044732;
  color += texture2D(image, uv - (off2 / _resolution)) * 0.09447039785044732;
  color += texture2D(image, uv + (off3 / _resolution)) * 0.010381362401148057;
  color += texture2D(image, uv - (off3 / _resolution)) * 0.010381362401148057;
  return color;
  return color;
}
uniform sampler2D vTex;
uniform float originX;
uniform float originY;
uniform float range;
uniform float zoom;
uniform float time;
uniform float blurStrength;

void main() {

  vec2 uv = gl_FragCoord.xy / resolution.xy;

  vec2 rect = vec2(uv.x * range + originX, uv.y * range + originY);
  vec4 texColor = texture2D(vTex, rect);

  vec2 direction = vec2(0.2, 0.2);
  vec2 rectResolution = vec2(resolution.x * range + resolution.x * originX,
                             resolution.y * range + resolution.y * originY);

  vec4 gaussian = blur13(vTex, rect, rectResolution.xy, direction);
  texColor = gaussian;

  vec4 renderColor = vec4(texColor.rgb, 0.5);
  vec3 grayColor = vec3(dot(texColor.rgb, monochromeScale));
  renderColor = vec4(powColor(grayColor, 2.0), 1.0);

  gl_FragColor = renderColor;
}
