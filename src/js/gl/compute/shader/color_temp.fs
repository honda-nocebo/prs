// =======================================
// ビデオテクスチャの保持(一瞬静止画的なやつ)
// =======================================

uniform float copyTex;

void main() {
  vec2 uv = gl_FragCoord.xy / resolution.xy;
  vec4 color = texture2D(textureColor, uv);
  vec4 before = texture2D(textureTempColor, uv);
  vec4 result = mix(before, color, copyTex);
  gl_FragColor = result;
}
