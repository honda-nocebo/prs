// =======================================
// rgbシフト
// =======================================

uniform float time;

void main() {

  vec2 uv = gl_FragCoord.xy / resolution.xy;

  float n1 = snoise(vec3(uv, time * 0.08));
  float n2 = snoise(vec3(uv, time * 0.07 + 512.0));
  float n3 = snoise(vec3(uv, time * 0.06 + 1024.0));

  vec2 offset = vec2(cos(n1 + n3), sin(n1 + n2));

  vec4 c01 = texture2D(textureMono, uv + offset * (n1 * 0.006));
  vec4 c02 = texture2D(textureMono, uv + offset * (n2 * 0.005));
  vec4 c03 = texture2D(textureMono, uv + offset * (n3 * 0.009));

  vec4 renderColor = vec4(c01.r, c02.g, c03.b, 1.0);

  gl_FragColor = renderColor;
}
