// =======================================
// ビデオテクスチャ
// =======================================
uniform float time;
uniform sampler2D vTex;

uniform float originX;
uniform float originY;
uniform float range;
uniform float zoom;

void main() {

  vec2 uv = gl_FragCoord.xy / resolution.xy;
  //  ----------------------------------
  float zoomV = 0.0;
  float rectX = uv.x * (range - zoom + zoomV) + originX + (zoom * 0.5 + zoomV);
  float rectY = uv.y * (range - zoom + zoomV) + originY + (zoom * 0.5 + zoomV);
  vec4 texColor = texture2D(vTex, vec2(rectX, rectY));
  // ----------------------------------
  gl_FragColor = vec4(texColor);
}
