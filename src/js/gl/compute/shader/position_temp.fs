// =======================================
// 前回位置 + textureVelocity分加算
// の(静止)保持用
// =======================================
uniform float copyTex;

void main() {
  vec2 uv = gl_FragCoord.xy / resolution.xy;
  vec4 position = texture2D(texturePosition, uv);
  vec4 before = texture2D(textureTempPosition, uv);
  vec4 result = mix(before, position, copyTex);
  gl_FragColor = result;
}
