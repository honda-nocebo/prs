// =======================================
// ここでフレーム毎の移動量決める
// =======================================

precision mediump float;
uniform float time;
uniform float seed;
uniform float flow01;
uniform float flow02;
uniform float referenceVelocity;
uniform float lim;
uniform float renderState;

void main() {
  vec2 uv = gl_FragCoord.xy / resolution.xy;
  vec3 position = texture2D(texturePosition, uv).xyz;
  float stepV = 0.2;
  float speed = mix(0.05, 0.4, referenceVelocity);

  vec3 velocity = curlNoise(vec3(position.xy * stepV + seed,
                                 position.z * stepV + seed + (flow02))) *
                  speed * (flow02 + 0.12);

  gl_FragColor = vec4(velocity * renderState, 1.0);
}
