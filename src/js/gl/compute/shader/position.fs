// =======================================
// パーティクルが実際に参照する位置情報
// 16 : 9の枠に収まる用に制御
// =======================================

uniform float time;

const float limX = 8.0;
const float limY = 4.5;
const float limZ = 6.0;
void main() {
  vec2 uv = gl_FragCoord.xy / resolution.xy;
  vec3 position = texture2D(texturePosition, uv).xyz;
  vec3 velocity = texture2D(textureVelocity, uv).xyz;

  // 16 : 9の枠
  vec3 result = position + velocity;
  result.x = mix(result.x, (snoise(position)) * limX, step(result.x, -limX));
  result.x = mix(result.x, (snoise(position)) * limX, step(limX, result.x));
  result.y = mix(result.y, (snoise(position)) * limY, step(result.y, -limY));
  result.y = mix(result.y, (snoise(position)) * limY, step(limY, result.y));
  result.z = mix(result.z, (snoise(position)) * limZ, step(result.z, -limZ));
  result.z = mix(result.z, (snoise(position)) * limZ, step(limZ, result.z));

  gl_FragColor = vec4((result), 1.0);
}
