// =======================================
// GPGPU的なものは基本ここでまとめて処理
// =======================================

import React, { useRef, useEffect, useCallback } from "react";
import * as THREE from "three";
import { useFrame, useThree } from "react-three-fiber";
import { updateValue } from "@/data";

import { GPUComputationRenderer } from "@/vendor/GPUComputationRenderer.js";
// npmからだとバージョンの問題っぽいのか何かエラーでちゃうので別途配置

import snoise from "@/gl/compute/shader/snoise.glsl"; // シンプレックスノイズ + カールノイズとか追記
import PositionShader from "@/gl/compute/shader/position.fs";
import VelocityShader from "@/gl/compute/shader/velocity.fs";
import ColorShader from "@/gl/compute/shader/color.fs";
import MonoShader from "@/gl/compute/shader/mono.fs";
import RbgShader from "@/gl/compute/shader/rgb.fs";
import TempColorShader from "@/gl/compute/shader/color_temp.fs";
import TempPositionShader from "@/gl/compute/shader/position_temp.fs";

import { Conf } from "@/index";

const Compute: React.FC<any> = (props) => {
  //   console.log("Compute");
  const { compute, setCompute, videoRef } = props;
  const { gl } = useThree();
  // ---------------------
  const elapsedTime = useRef<any>(0);
  // ---------------------
  const videoTexture: THREE.VideoTexture[] = new Array(Conf.VIDEO_TEXTURE_NUM)
    .fill(0)
    .map((v: any, i: any) => new THREE.VideoTexture(videoRef[i].current));

  useEffect(() => {
    const computeRenderer = new GPUComputationRenderer(Conf.TEXTURE_WIDTH, Conf.TEXTURE_HEIGHT, gl);
    const position = computeRenderer.createTexture();
    const velocity = computeRenderer.createTexture();
    const tempPosition = computeRenderer.createTexture();
    const color = computeRenderer.createTexture();
    const mono = computeRenderer.createTexture();
    const rgb = computeRenderer.createTexture();
    const tempColor = computeRenderer.createTexture();

    const textureArraySize = Conf.TEXTURE_WIDTH * Conf.TEXTURE_HEIGHT * 4;

    // postion初期位置
    for (let i = 0; i < textureArraySize; i += 4) {
      position.image.data[i] = Math.random() * 16.0 - 8.0;
      position.image.data[i + 1] = Math.random() * 9.0 - 4.5;
      position.image.data[i + 2] = Math.random() * 6.0 - 3.0;
    }

    // initで作った一次元配列渡す
    const texturePosition = computeRenderer.addVariable("texturePosition", `${snoise} ${PositionShader}`, position);
    const textureVelocity = computeRenderer.addVariable("textureVelocity", `${snoise} ${VelocityShader}`, velocity);
    const textureTempPosition = computeRenderer.addVariable("textureTempPosition", `${TempPositionShader}`, tempPosition);
    const textureColor = computeRenderer.addVariable("textureColor", `${snoise} ${ColorShader}`, color);
    const textureMono = computeRenderer.addVariable("textureMono", `${MonoShader}`, mono);
    const textureRgb = computeRenderer.addVariable("textureRgb", `${snoise} ${RbgShader}`, rgb);
    const textureTempColor = computeRenderer.addVariable("textureTempColor", `${TempColorShader}`, tempColor);

    // テクスチャ参照の依存関係を設定
    computeRenderer.setVariableDependencies(texturePosition, [texturePosition, textureVelocity]);
    computeRenderer.setVariableDependencies(textureVelocity, [texturePosition, textureVelocity]);
    computeRenderer.setVariableDependencies(textureTempPosition, [texturePosition, textureTempPosition]);
    computeRenderer.setVariableDependencies(textureColor, [texturePosition, textureColor, textureTempColor]);
    computeRenderer.setVariableDependencies(textureMono, [textureMono]);
    computeRenderer.setVariableDependencies(textureRgb, [textureRgb, textureMono]);
    computeRenderer.setVariableDependencies(textureTempColor, [texturePosition, textureColor, textureTempColor]);

    texturePosition.material.uniforms = {
      time: { value: 0.0 },
    };
    textureVelocity.material.uniforms = {
      time: { value: 0.0 },
      delta: { value: 0.0 },
      seed: { value: 0.0 },
      flow01: { value: 0.0 },
      flow02: { value: 0.0 },
      referenceVelocity: { value: 0.0 },
      speedToggle: { value: 0.0 },
      lim: { value: 0.0 },
      renderState: { value: 0.0 },
    };
    textureTempPosition.material.uniforms = {
      copyTex: { value: 0.0 },
    };
    textureColor.material.uniforms = {
      time: { value: 0.0 },
      seed: { value: 0.0 },
      vTex: { value: videoTexture[1] }, //ビデオテクスチャ
      originX: { value: 0.0 }, //切り取る画角の基点X
      originY: { value: 0.0 }, //切り取る画角の基点Y
      range: { value: 0.0 }, //切り取る範囲
      zoom: { value: 0.0 }, //ズーム用
    };
    textureMono.material.uniforms = {
      time: { value: 0.0 },
      vTex: { value: 0.0 },
      range: { value: 0.0 },
      originX: { value: 0.0 },
      originY: { value: 0.0 },
      zoom: { value: 0.0 },
      blurStrength: { value: 0.0 },
    };
    textureRgb.material.uniforms = {
      time: { value: 0.0 },
    };
    textureTempColor.material.uniforms = {
      copyTex: { value: 0.0 },
    };

    const computeRendererError = computeRenderer.init();
    if (computeRendererError) {
      console.error("ERROR", computeRendererError);
    }

    setCompute({
      isSet: true,
      computeRenderer: computeRenderer,
      videoTexture: videoTexture,
      texturePosition: texturePosition,
      textureVelocity: textureVelocity,
      textureTempPosition: textureTempPosition,
      textureColor: textureColor,
      textureMono: textureMono,
      textureRgb: textureRgb,
      textureTempColor: textureTempColor,
    });
  }, []);

  // ----------------------

  useFrame((frame) => {
    // elapsedTime.current = frame.clock.elapsedTime;

    update();
    if (updateValue.videoState == "play") {
      compute.textureVelocity.material.uniforms.renderState.value = 1.0;
      // なんかこうしないとレンダリングのコマ送りで
      // computeRendererがうまく動かない?ので
      // (updateはし続けてベロシティ係数を0.0,1.0でトグルしてる)
    }
    if (updateValue.renderState) {
      elapsedTime.current += 0.03;
      compute.textureVelocity.material.uniforms.renderState.value = 1.0;
      updateValue.renderState = false;
    }
  });

  const update = useCallback(() => {
    // ---------------
    compute.computeRenderer.compute();
    // ---------------
    if (compute.textureVelocity.material.uniforms.renderState.value == 1.0) {
      updateValue.flow01 = updateValue.flow01 <= 0 ? 0.001 : updateValue.flow01 - 0.095;
      updateValue.flow02 = updateValue.flow02 <= 0.03 ? 0.001 : updateValue.flow02 - 0.03;
      updateValue.zoom = updateValue.zoom <= 0.009 ? 0.009 : updateValue.zoom - 0.04;
      updateValue.lim = updateValue.lim <= 0.0 ? 0.0 : updateValue.lim - 0.04;
      updateValue.referenceColor = updateValue.referenceColor >= 1.0 ? 1.0 : updateValue.referenceColor;
      updateValue.bgOpacity = updateValue.bgOpacity >= 1.0 ? 1.0 : updateValue.bgOpacity;

      if (updateValue.kick02State) {
        updateValue.blurStrength = updateValue.lim >= 1.5 ? 1.5 : updateValue.blurStrength + 0.01;
      }
    }

    // ---------------
    compute.texturePosition.material.uniforms.time.value = elapsedTime.current;
    // ---------------
    compute.textureVelocity.material.uniforms.time.value = elapsedTime.current;
    compute.textureVelocity.material.uniforms.seed.value = updateValue.seed;
    compute.textureVelocity.material.uniforms.flow01.value = updateValue.flow01;
    compute.textureVelocity.material.uniforms.flow02.value = updateValue.flow02;
    compute.textureVelocity.material.uniforms.referenceVelocity.value = updateValue.referenceVelocity;
    compute.textureVelocity.material.uniforms.speedToggle.value = updateValue.speedToggle;
    compute.textureVelocity.material.uniforms.lim.value = updateValue.lim;
    // ---------------
    compute.textureColor.material.uniforms.time.value = elapsedTime.current;
    compute.textureColor.material.uniforms.vTex.value = videoTexture[updateValue.videoId];
    compute.textureColor.material.uniforms.originX.value = updateValue.originX;
    compute.textureColor.material.uniforms.originY.value = updateValue.originY;
    compute.textureColor.material.uniforms.range.value = updateValue.range;
    compute.textureColor.material.uniforms.zoom.value = updateValue.zoom;
    // ---------------
    compute.textureMono.material.uniforms.time.value = elapsedTime.current;
    compute.textureMono.material.uniforms.vTex.value = compute.videoTexture[updateValue.videoId];
    compute.textureMono.material.uniforms.range.value = updateValue.range;
    compute.textureMono.material.uniforms.originX.value = updateValue.originX;
    compute.textureMono.material.uniforms.originY.value = updateValue.originY;
    compute.textureMono.material.uniforms.blurStrength.value = updateValue.blurStrength;
    // ---------------
    compute.textureRgb.material.uniforms.time.value = elapsedTime.current;
    // ---------------

    if (updateValue.copyTex) {
      updateValue.copyTex = false;
      compute.textureTempColor.material.uniforms.copyTex.value = 1.0;
      compute.textureTempPosition.material.uniforms.copyTex.value = 1.0;
    } else {
      compute.textureTempColor.material.uniforms.copyTex.value = 0.0;
      compute.textureTempPosition.material.uniforms.copyTex.value = 0.0;
    }
    // ---------------
    compute.textureVelocity.material.uniforms.renderState.value = 0.0;
    // コマ送り用の対策
  }, [compute, setCompute]);
  return <></>;
};

export default Compute;
