// =======================================
// computeからのテクスチャ参照
// =======================================

precision mediump float;
varying vec2 vUv;
uniform sampler2D textureRgb;
uniform float bgOpacity;

void main() {
  vec4 texColor = texture2D(textureRgb, vUv);
  vec4 renderColor = vec4(texColor.rgb, bgOpacity);
  gl_FragColor = vec4(renderColor);
}
