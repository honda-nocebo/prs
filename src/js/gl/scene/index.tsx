// =======================================
// 背景の動画
// =======================================
import React, { useRef, useEffect, useMemo } from "react";
import * as THREE from "three";
import { useFrame } from "react-three-fiber";
import { updateValue } from "@/data";

import { Conf } from "@/index";

import snoise from "@/gl/compute/shader/snoise.glsl";
import Fragment from "@/gl/scene/shader/fragment.fs";
const FragmentShader = `${snoise} ${Fragment}`;

const Scene: React.FC<any> = (props) => {
  //   console.log("Scene");
  const { compute } = props;
  const material: any = useRef();
  const mesh: any = useRef();

  const args = useMemo(() => {
    return {
      uniforms: {
        textureRgb: { type: "t", value: null },
        time: { type: "f", value: 0.0 },
        bgOpacity: { type: "f", value: 0.0 },
        resolution: { type: "v2", value: new THREE.Vector2(Conf.TEXTURE_WIDTH, Conf.TEXTURE_HEIGHT) },
      },
      vertexShader: `
            varying vec2 vUv;
              void main() {
                  vUv = uv;
                  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
              }  
            `,
      fragmentShader: FragmentShader,
    };
  }, []);

  useFrame((frame) => {
    if (!material.current) return;

    material.current.uniforms.time.value = frame.clock.elapsedTime;
    material.current.uniforms.bgOpacity.value = updateValue.bgOpacity;

    material.current.uniforms.textureRgb.value = compute.computeRenderer.getCurrentRenderTarget(compute.textureRgb).texture;
  });
  useEffect(() => {
    if (!mesh.current) return;
    mesh.current.position.z = -0.05;
    // z軸ちょっとずらしてパーティクルとかぶらなように
  }, []);

  return (
    <mesh ref={mesh}>
      <planeBufferGeometry attach="geometry" args={[16, 9]} />
      <shaderMaterial attach="material" ref={material} args={[args]} transparent={true} />
    </mesh>
  );
};

export default Scene;
