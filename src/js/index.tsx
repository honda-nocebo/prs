// =======================================
// メインの呼び出し元
// =======================================

import React, { useRef, createContext, useState } from "react";
import ReactDOM from "react-dom";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
library.add(fab, fas, far);

import { Reset } from "styled-reset";
import styled from "styled-components";
import "../css/style.scss";

import GL from "@/gl/index";
import StreamLoader from "@/media/streamLoader";
import Video from "@/media/video";
import Audio from "@/media/audio";
import Recorder from "@/media/recorder";
import UI from "@/ui/";
import Data from "@/data/";

// console.log(process.env.APP_URL);
// console.log(process.env.NODE_ENV);
// =======================================
export const Conf: any = {
  VIDEO_TEXTURE_NUM: 5,
  GL_SIZE_FIX: false, //サイズ固定
  QUALITY: "HIGH",
  TEXTURE_WIDTH: 1920,
  TEXTURE_HEIGHT: 1080,
  PARTICLE_COUNT: 1920 * 1080,
  NORMAL: {
    TEXTURE_WIDTH: 1280,
    TEXTURE_HEIGHT: 720,
    PARTICLE_COUNT: 1280 * 720,
  },
  HIGH: {
    TEXTURE_WIDTH: 1920,
    TEXTURE_HEIGHT: 1080,
    PARTICLE_COUNT: 1920 * 1080,
  },
};
// =======================================

const obj: any = {};
export const ResourceContext = createContext([obj, () => {}]);

const Content: React.FC = () => {
  //   console.log("Content");
  const [resource, setResource] = useState({
    videoState: "pause",
    isDataShow: false,
    canvas: null,
    audio: null,
  });

  const videoRef = new Array(Conf.VIDEO_TEXTURE_NUM).fill(0).map(() => useRef<HTMLVideoElement>());
  const [videoBuffer, setVideoBuffer] = useState<Uint8Array[]>();
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [isReady, setIsReady] = useState<boolean>(false);

  return !isLoaded ? (
    <StreamLoader setIsLoaded={setIsLoaded} setVideoBuffer={setVideoBuffer} />
  ) : (
    <Root>
      <Video videoRef={videoRef} videoBuffer={videoBuffer} setIsReady={setIsReady} />
      {isReady && (
        <ResourceContext.Provider value={[resource, setResource]}>
          <Data videoRef={videoRef} />
          <UI videoRef={videoRef} />
          <Audio videoRef={videoRef} />
          <GL videoRef={videoRef} />
          {process.env.NODE_ENV == "development" && <Recorder videoRef={videoRef} />}
        </ResourceContext.Provider>
      )}
    </Root>
  );
};
// =======================================
const Container = styled.div`
  width: 100%;
  height: 100%;
`;

const Root = styled.div`
  width: 100%;
  height: 100%;
`;

ReactDOM.render(
  <Container>
    <Reset />
    <Content />
  </Container>,
  document.getElementById("root")
);
// --------------------
