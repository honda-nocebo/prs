<?php

/*
=========================================
サーバーから動画配信するやつ
読み込み速度そこそこな気がするので
結局分割とかはしてない。
=========================================
*/

ini_set('error_reporting', E_ALL);
ini_set('display_errors', "On");

// クロスオリジン
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

// dotenv
require_once '../../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . "/../../../");
$dotenv->load();
$STORAGE_PATH = getenv('STORAGE_PATH');


$video = array(
    '0' => 'a',
    '1' => 'b',
    '2' => 'c',
    '3' => 'd',
    '4' => 'e',
);
$quality = array(
    'HIGH' => '1080',
    'NORMAL' => '720',
);

$id = $video[$_POST['id']];
$q = $quality[$_POST['quality']];


$file = $_SERVER['DOCUMENT_ROOT'] . $STORAGE_PATH . "{$id}_{$q}p.webm";
$fp = @fopen($file, 'rb');

$size   = filesize($file); // File size
$length = $size;           // Content length
$start  = 0;               // Start byte
$end    = $size - 1;       // End byte

// ローダー用にlengthだけ返す場合
if (isset($_POST['isReadLength'])) {
    echo $length;
    exit();
}

// =============================
// Content-Range返す
// (結局分割してない)
// =============================
header('Content-type: video/webm');
header("Accept-Ranges: 0-$length");

if (isset($_SERVER['HTTP_RANGE'])) {

    $c_start = $start;
    $c_end   = $end;

    list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
    if (strpos($range, ',') !== false) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        header("Content-Range: bytes $start-$end/$size");
        exit;
    }
    if ($range == '-') {
        $c_start = $size - substr($range, 1);
    } else {
        $range  = explode('-', $range);
        $c_start = $range[0];
        $c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
    }
    $c_end = ($c_end > $end) ? $ensd : $c_end;
    if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        header("Content-Range: bytes $start-$end/$size");
        exit;
    }
    $start  = $c_start;
    $end    = $c_end;
    $length = $end - $start + 1;
    fseek($fp, $start);
    header('HTTP/1.1 206 Partial Content');
}

header("Content-Range: bytes $start-$end/$size");
header("Content-Length: " . $length);

$buffer = 1024 * 8;
while (!feof($fp) && ($p = ftell($fp)) <= $end) {
    if ($p + $buffer > $end) {
        $buffer = $end - $p + 1;
    }
    // set_time_limit(0);
    echo fread($fp, $buffer);
    flush();
}

fclose($fp);
exit();
