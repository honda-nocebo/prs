<?php

/*
=========================================
jsから受け取ったbase64をjpgに変換
手動でffmpegで動画にする

/storage/のパーミッション要変更
upload max size / サーバー側のmax_sizeも要確認
=========================================
*/

ini_set('error_reporting', E_ALL);
ini_set('display_errors', "On");

// クロスオリジン
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');

// dotenv
require_once '../../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . "/../../../");
$dotenv->load();
$TEMP_PATH = getenv('TEMP_PATH');


$base64 = $_POST['base64'];
$currentFrame = $_POST['currentFrame'];

$file_path = $_SERVER['DOCUMENT_ROOT'] . $TEMP_PATH . $currentFrame  . ".jpg";


$new_file_name = $file_path;
$fp = fopen($new_file_name, 'w');
fwrite($fp, base64_decode($base64));
fclose($fp);

echo json_encode($file_path);
